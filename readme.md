# SQL schema migration generator
This bash script starts a docker container with postgresql:9.6, creates two databases with given schema and comapres them.

# Requirements
1. Docker (https://www.docker.com/)
1. Migra (https://github.com/djrobstep/migra)

# Usage
    ./migrator.sh <path_to_new_schema.sql>

# Result
Old database is created using sql files in `migrations` directory. New database is created using new schema file passed as argument to this script.
Resulting migration is saved as SQL file to `migrations` directory. 
