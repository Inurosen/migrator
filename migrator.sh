#!/bin/bash

if [ ! -f $1 ]; then
	echo -e "\e[31;1mFile ${1} not found\e[0m"
	exit
fi
echo -e "\e[93;1mStarting PostgreSQL container...\e[0m"

POSTGRES_USER=migrator
POSTGRES_PASSWORD=TopS3cr3t
DATABASE_OLD=db_old
DATABASE_NEW=db_new

docker run -v `pwd`/migrations:/migrations -v `realpath $1`:`realpath $1` --name migrator_db -h migrator_db -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -d postgres:9.6
echo -e "\e[93;1mWaiting for server...\e[0m"

sleep 6

MIGRATOR_IP=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' migrator_db`

echo -e "\e[93;1mContainer IP:\e[0m \e[15;1m${MIGRATOR_IP}\e[0m"

echo -e "\e[93;1mCreating OLD database...\e[0m"
docker exec -it migrator_db createdb -U $POSTGRES_USER -O $POSTGRES_USER $DATABASE_OLD
for f in ./migrations/*.sql; do
	docker exec -it migrator_db psql --user=$POSTGRES_USER $DATABASE_OLD --single-transaction -f /migrations/`basename $f`
done
docker exec -it migrator_db psql --user=$POSTGRES_USER $DATABASE_OLD --command="\dt"

echo -e "\e[93;1mCreating NEW database...\e[0m"
docker exec -it migrator_db createdb -U $POSTGRES_USER -O $POSTGRES_USER $DATABASE_NEW
docker exec -it migrator_db psql --user=$POSTGRES_USER $DATABASE_NEW --single-transaction -f `realpath $1`
docker exec -it migrator_db psql --user=$POSTGRES_USER $DATABASE_NEW --command="\dt"

echo -e "\e[93;1mMaking diff...\e[0m"
OUTPUT_FILE=sql_`date +%Y%m%d_%H%M%S`.sql
MIGRATION_DIFF=`migra --unsafe postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$MIGRATOR_IP:5432/$DATABASE_OLD postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$MIGRATOR_IP:5432/$DATABASE_NEW`

if [ ${#MIGRATION_DIFF} -eq 0 ]; then
	echo -e "\e[15;1mDatabases are identical"
else
	echo "${MIGRATION_DIFF}" > migrations/$OUTPUT_FILE
	echo -e "\e[15;1m"
	echo "${MIGRATION_DIFF}"
	echo -e "\e[0m"
fi

echo -e "\e[93;1mCleaning up...\e[0m"

docker stop migrator_db > /dev/null
docker rm -v migrator_db > /dev/null
